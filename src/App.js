import './App.css';
import imahe from './glasscup.jpeg';
// import 'bootstrap/dist/css/bootstrap.min.css';
import { Button } from 'reactstrap';


function App() {
  return (
    <div className="App">
      <article className="card">
        <div className="card__wrapper">

          <figure className="card__feature">
            <img src={imahe} class="card__img" alt="Glass Cup" />
          </figure>

          <div class="card__box">
            <header class="card__item card__header">
            <div className='title card__title'>Glass Cup
            <Button className='button1' color='primary' size='sm'>Edit</Button>
            </div>
            </header>
            <section class="card__item card__body">
              <h6 className='content'>The latest stylish, practical and durable.</h6>

              <Button className='button2' color='danger' size='sm'>Delete Item</Button>
              <Button className='button3' color='success' size='sm'>Publish</Button>

            </section>
            

          </div>

        </div>
      </article>


    </div>

  );
}

export default App;
